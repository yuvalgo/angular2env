/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { InvoicesService } from './invoices.service';

describe('Service: Invoice', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [InvoiceService]
    });
  });

  it('should ...', inject([InvoicesService], (service: InvoicesService) => {
    expect(service).toBeTruthy();
  }));
});
