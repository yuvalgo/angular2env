import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Invoice} from '../invoices/invoice';
import {InvoicesService} from './invoices.service';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class InvoicesComponent implements OnInit {
  invoices;
  
  currentInvoice;
  isLoading = true;
 select(invoice){
		this.currentInvoice = invoice; 
    //console.log(	this.currentUser);
 }
  constructor(private _InvoiceService: InvoicesService) {
  }

  addInvoice(invoice){//roni class
    console.log("p com");
    this._InvoiceService.addInvoice(invoice);
  }

  ngOnInit() {
 setTimeout(() => 
{
        this._InvoiceService.getInvoices()
			    .subscribe(
            
            invoices => {this.invoices = invoices;
                               this.isLoading = false;
                               console.log(invoices)});},
5000);   

  }

}
