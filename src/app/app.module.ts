import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { UsersComponent } from './users/users.component';
import { DemoComponent } from './demo/demo.component';
import { UsersService } from './users/users.service';
import { UserComponent } from './user/user.component';
import { SpinnerComponent } from './shared/spinner/spinner.component';
import { PostsComponent } from './posts/posts.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { UserFormComponent } from './user-form/user-form.component';
import {AngularFireModule} from 'angularfire2';

import { ProductComponent } from './product/product.component';
import { ProductsComponent } from './products/products.component';
import { ProductsService } from './products/products.service';
import { ProductFormComponent } from './product-form/product-form.component';

import { InvoicesComponent } from './invoices/invoices.component';
import { InvoiceFormComponent } from './invoice-form/invoice-form.component';
import { InvoicesService } from './invoices/invoices.service';

export const firebaseConfig = {
    apiKey: "AIzaSyC1wVBkRpdhM6aZGELzw2gGkjxiWDQJJC4",
    authDomain: "yuvalgo-c7146.firebaseapp.com",
    databaseURL: "https://yuvalgo-c7146.firebaseio.com",
    storageBucket: "yuvalgo-c7146.appspot.com",
    messagingSenderId: "631851749539"
}

const appRoutes:Routes =[

  {path:'',component:InvoiceFormComponent},
  {path:'invoices',component:InvoicesComponent},
  {path:'invoiceForm',component:InvoiceFormComponent},
  {path:'users',component:UsersComponent},
  {path:'posts',component:PostsComponent},

  {path:'products',component:ProductsComponent},

  //{path:'',component:UsersComponent},
  {path:'**',component:PageNotFoundComponent},

] 

@NgModule({
  declarations: [
    AppComponent,
    UsersComponent,
    DemoComponent,
    UserComponent,
    SpinnerComponent,
    PostsComponent,
    PageNotFoundComponent,
    UserFormComponent,
    ProductComponent,
    ProductsComponent,
    ProductFormComponent,
    InvoicesComponent,
    InvoiceFormComponent,
    

  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig)
  ],
  providers: [UsersService, ProductsService, InvoicesService],
  bootstrap: [AppComponent]
})
export class AppModule { }
