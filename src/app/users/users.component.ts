import { Component, OnInit } from '@angular/core';
import {UsersService} from './users.service';

@Component({
  selector: 'jce-users',
  templateUrl: './users.component.html',
  styles: [`
    .users li { cursor: default; }
    .users li:hover { background: #ecf0f1; }
    .list-group-item.active, 
    .list-group-item.active:hover { 
         background-color: #ecf0f1;
         border-color: #ecf0f1; 
         color: #2c3e50;
    }     
  `]
})
export class UsersComponent implements OnInit {

 users;

 currentUser;

 isLoading = true;

  constructor(private _usersService: UsersService) {
    //this.users = this._userService.getUsers();
  }
 select(user){
		this.currentUser = user; 
    //console.log(	this.currentUser);
 }
/*  editUser(user){//roni bitbucket
     this._usersService.updateUser(user); 
  }  */  
/*  deleteUser(user){ //roni bitbucket
      this._usersService.deleteUser(user);
  }*/

  addUser(user){//roni class
    this._usersService.addUser(user);
  }
  updateUser(user){//roni class
    this._usersService.updateUser(user);
  }
  deleteUser(user){  //roni class
    this._usersService.deleteUser(user);
  }

  


  ngOnInit() {
        this._usersService.getUsers()
			    .subscribe(users => {this.users = users;
                               this.isLoading = false;
                               console.log(users)});
  }

}