import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import {Invoice} from '../invoices/invoice';
import {NgForm} from '@angular/forms';
import {InvoicesService} from '../invoices/invoices.service';

@Component({
  selector: 'app-invoice-form',
  templateUrl: './invoice-form.component.html',
  styleUrls: ['./invoice-form.component.css']
})
export class InvoiceFormComponent implements OnInit {

  //addInvoice(invoice);


  //@Output() invoiceAddedEvent = new EventEmitter<Invoice>();
  invoice:Invoice = {name:'',amount:'1000'};

  
  constructor(private _InvoiceService: InvoicesService) {
  }
  onSubmit(form:NgForm){
    console.log(form);
    this._InvoiceService.addInvoice(this.invoice);
    //this.invoiceAddedEvent.emit(this.invoice);
    this.invoice = {
      name:'',
      amount:''
    }
  }
  ngOnInit() {
  }

}
